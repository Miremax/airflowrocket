# import time
from datetime import datetime, timedelta

import pendulum

from airflow import DAG

# from airflow.models import Variable

# from airflow.utils.task_group import TaskGroup

# from airflow.sensors.external_task import ExternalTaskSensor

# from airflow.operators.bash import BashOperator
from airflow.operators.empty import EmptyOperator
from airflow.operators.python import PythonOperator
# from airflow.operators.trigger_dagrun import TriggerDagRunOperator
# from airflow.providers.postgres.operators.postgres import PostgresOperator
# from airflow_clickhouse_plugin.operators.clickhouse_operator import ClickHouseOperator

# from airflow.hooks.base import BaseHook
# from airflow.providers.amazon.aws.hooks.s3 import S3Hook
# from airflow.providers.postgres.hooks.postgres import PostgresHook
# from airflow_clickhouse_plugin.hooks.clickhouse_hook import ClickHouseHook


# Конфигурация DAG
OWNER = 'i.korsakov'
DAG_ID = 'test_templates'
LOCAL_TZ = pendulum.timezone('Europe/Moscow')

# Используемые таблицы в DAG
GP_TARGET_SCHEMA = ''
GP_TARGET_TABLE = ''
GP_TMP_SCHEMA = ''
GP_TMP_TABLE = ''
GP_ET_SCHEMA = ''
GP_ET_TABLE = ''

# Названия коннекторов к GP
GP_CONNECT_DEV = ''
GP_CONNECT_PROD = ''

LONG_DESCRIPTION = '''
# LONG DESCRIPTION
'''

SHORT_DESCRIPTION = 'SHORT DESCRIPTION'


# Описание возможных ключей для default_args
# https://github.com/apache/airflow/blob/343d38af380afad2b202838317a47a7b1687f14f/airflow/example_dags/tutorial.py#L39
args = {
    'owner': OWNER,
    'start_date': datetime(2023, 1, 20, tzinfo=LOCAL_TZ),
    'catchup': True,
    'retries': 3,
    'retry_delay': timedelta(hours=1),
    # 'sla': timedelta(seconds=1),
    # 'sla_miss_callback': sla_callback,
}


def print_airflow_templates(**context):
    """"""

    exclude_keys = ('execution_date', 'next_ds', 'next_ds_nodash', 'next_execution_date', 'prev_ds', 'prev_ds_nodash',
                    'prev_execution_date', 'prev_execution_date_success', 'tomorrow_ds', 'tomorrow_ds_nodash',
                    'yesterday_ds', 'yesterday_ds_nodash')

    for context_value in context:
        if context_value not in exclude_keys:
            print(f'key_name – {context_value} | '
                  f'value_name – {context[context_value]} | '
                  f'type_value_name – {type(context[context_value])}')


with DAG(
        dag_id=DAG_ID,
        schedule_interval='10 0 * * *',
        default_args=args,
        tags=['templates', 'test'],
        description=SHORT_DESCRIPTION,
        concurrency=1,
        max_active_tasks=1,
        max_active_runs=1,
) as dag:
    dag.doc_md = LONG_DESCRIPTION

    start = EmptyOperator(
        task_id='start',
    )

    print_airflow_templates = PythonOperator(
        task_id='print_airflow_templates',
        python_callable=print_airflow_templates,
        # sla=timedelta(seconds=2)
    )

    # test_context_format = BashOperator(
    #     task_id='test_context_format',
    #     bash_command='''echo {{ prev_data_interval_start_success.format('YYYY-MM') }}'''
    # )

    # test_context_format_1 = BashOperator(
    #     task_id='test_context_format_1',
    #     bash_command=f'''echo "DELETE FROM test '{{{{ prev_data_interval_start_success.format('YYYY-MM-DD') }}}}' "'''
    # )

    # with TaskGroup(group_id=f'') as task_group:
    #     """
    #
    #     """
    #
    #     start_make_vacuum_table = EmptyOperator(
    #         task_id='',
    #     )
    #
    #     end_make_vacuum_table = EmptyOperator(
    #         task_id='',
    #     )
    #
    #     for schema in get_actual_list_schema():
    #         for table in get_actual_list_tables(name_schema=schema):
    #             make_table_vacuum = PythonOperator(
    #                 task_id=f't_{schema}_{table}',
    #                 python_callable=make_vacuum_table,
    #                 op_kwargs={
    #                     'table_schema': schema,
    #                     'table_name': table
    #                 },
    #             )
    #
    #             start_make_vacuum_table >> make_table_vacuum >> end_make_vacuum_table

    # sensor = ExternalTaskSensor(
    #     task_id=f'sensor_{dag}',
    #     external_dag_id=dag,
    #     allowed_states=['success'],
    #     mode='reschedule',
    #     timeout=360000,  # длительность работы сенсора
    #     poke_interval=600  # частота проверки
    # )

    # trigger_dag_morning_message_08_00 = TriggerDagRunOperator(
    #     task_id='trigger_dag_morning_message_08_00',
    #     trigger_dag_id='fct_dm_morning_message_to_pgsql',
    #     execution_date='{{ data_interval_start }}'
    #
    # )

    end = EmptyOperator(
        task_id='end',
    )

    start >> print_airflow_templates >> end
