import airflow
from airflow import DAG
from airflow.operators.bash import BashOperator
from airflow.operators.python import PythonOperator


import requests as rq



dag = DAG(
    dag_id="jokes_to_cmd",
    start_date=airflow.utils.dates.days_ago(14),
    schedule_interval==timedelta(minutes=1),
)

def _get_joke:
    joke = rq.get('https://official-joke-api.appspot.com/random_joke').json()
    with open(f'joke {joke["id"]}.txt', 'w') as the_file:
        the_file.write(joke['setup']+'\n')
        the_file.write(joke['punchline'])
    print(joke['id'], joke['setup'], joke['punchline'])

get_joke = PythonOperator(
    task_id="get_joke",
    python_callable=_get_joke,
    dag=dag,
)

notify = BashOperator(
    task_id='notify',
    bash_command='echo "Hello, Airflow!"',
    dag=dag
)


get_joke >> notify