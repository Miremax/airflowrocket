FROM apache/airflow:2.4.3-python3.10


RUN pip3 install --upgrade pip && \
    pip3 install  --no-cache-dir \
    airflow-clickhouse-plugin==0.10.0 \
    fpdf==1.7.2 \
    matplotlib==3.5.1 \
    openpyxl==3.0.9 \
    gspread==5.7.2 \
    minio==7.1.13 \
    boto3==1.24.28 \
    s3fs==0.4.0 \
    tapi_yandex_metrika==2022.4.8 \
    clickhouse-connect==0.5.15 \
    gspread-dataframe==3.3.0 \
